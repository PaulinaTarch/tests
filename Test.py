import unittest

from selenium import webdriver

class MainTest(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.driver = webdriver.Chrome(executable_path=r"C:\Users\Asus\Desktop\kurs_selenium\chromedriver.exe")

    def test_main_page(self):
        driver = self.driver
        driver.get('https://www.vectorizer.io/?ref=discuvver')
        title = driver.title
        print(title)
        assert 'Online Image Vectorizer' == title

    def test_expamples_page(self):
        driver = self.driver
        driver.get('https://www.vectorizer.io/examples/')
        title = driver.title
        print(title)
        assert 'Online Image Vectorizer - Examples' == title

    @classmethod
    def tearDownClass(self):
        self.driver.quit()


